# Inventaires XML/EAD3 de la Fondation Jean Monnet

Ces inventaires décrivent les archives conservées par la [Fondation Jean Monnet pour l'Europe](https://jean-monnet.ch) à Lausanne en Suisse.

## Format
Ces fichiers suivent la grammaire XML EAD3 (Encoded Archival Description version 3) telle que décrite [ici](https://www.loc.gov/ead).

## Licence
Ces documents sont distribués sous la licence Creative Commons [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0).
